
\version "2.18.2"
\language "english"

\header {
  title = "Olive Me"
  subsubtitle = ""
  composer = "Johnny Legendary"
  tagline = ""
}

\paper {
  top-system-spacing #'basic-distance = #10
  score-system-spacing #'basic-distance = #20
  system-system-spacing #'basic-distance = #20
  last-bottom-spacing #'basic-distance = #10
}

global = {
  \key ef \major
  \time 4/4
}

soloMusic = \relative c' {
  % intro
  r1 r r r
  % verse 1
  r2 f8 g g4 | g8 g g4 g ef | ef r f8 g g4 | g8 f g4 g8 f ef ef( |
  c4) r8 c g' g4. | af2 g8 ef4. | af2 g8 ef4 ef8 | f2 g8 f4. |
  c4 r 
  g'8 g g4 | g8 f g4 g8 f ef ef~ | ef4 r g8 g g4 | bf8 af g4 g8 f ef ef( | c4) r4
  g'8 g4. | af2 g8 ef4 ef8 | af4 af g8 ef4 ef8 
}

soloWords = \lyricmode {
  What would I do with -- out your smart mouth,
  draw -- ing me in and you kick -- ing me out.
  You got my head spin -- ning, no kid -- ding,
  I can't pin you down. 
  What's go -- ing on in that beau -- ti -- ful mind?
  I'm on your ma -- gi -- cal my -- ste -- ry ride.
  And I'm so diz -- zy, don't know what hit me,
  but
}

sopMusic = \relative c' {
  % intro
  r1 r | r r
  % verse 1
  r1 r r r r r r r r r r r r r r 
  % bridge
  f2 g8 f4. | af2 r4 r8 
  f8 | c'4. bf8~ bf af4 g8~ | g4. f8~ f ef4 d8~ | d4. c8~ c bf4 c8~ | c4 r4 r2
  c'4. bf8~ bf bf af g~ | g4. f8~ f f ef f~ | f2 r4
  % chorus
  g4 | bf2. g8 c8~ | c4 r4 g2 | f2. ef8 g8~ | g4 r
  g8 g4. | f4 f f ef8 f | c4 r g'8 g4. | f4 f f ef8 f | f4 r
  g8 g4. | bf2. g8 c~ | c4 r8 g c g4.| f2. ef8 g~ | g4 r
  g8 g4. | \break f4 f f ef8 f | c4 r g'8 g4. | f4 f f ef8 f | f4 r8
  g8 g af bf ef~( | ef4. d8~ d4. c8~ | c4. bf8~ bf4) g8 g~ | g4 r4 r2 | r4 r8
  g8 g af bf ef~ | ef4. d8~ d4. c8~ | c4. bf8~ bf4 g8 g~ | g4 r4 r4 g8( f~ | f2) r2
}

sopWords = \lyricmode {
  \repeat unfold 75 { \skip 1 }
  Cause I give you
  \repeat unfold 3 { \skip 1 }
  And you give me
}

altoMusic = \relative c' {
  % intro
  \repeat volta 2 { c4. c c4 af4. af af4 | bf4. bf bf4 bf4. bf bf4 } \break
  % verse 1
  r1 r r r r r r r r r r r r r r \break
  f'2 g8 f4. f2 r4 r8 
  f8 | af4. g8~ g f4 ef8~ | ef4. d8~ d c4 bf8~ | bf4. af8~ af g4 af8~ | af4 r4 r2
  af'4. g8~ g g8 f ef~ | ef4. d8~ d d c d~ | d2 r4
  % chorus
  g | g2. ef8 g~ | g4 r ef2 | ef2. ef8 ef~ | ef4 r
  g8 g4. | f4 f f ef8 f | c4 r g'8 g4. | f4 f f ef8 f | f4 r
  g8 g4. | g2. ef8 g~ | g4 r8 ef ef ef4. | ef2. ef8 ef~ | ef4 r
  g8 g4. | f4 f f ef8 f | c4 r g'8 g4. | f4 f f ef8 f | f4 r4 r4 r8
  g8~( | g4. f8~ f4. ef8~ | ef4. ef8~ ef4) ef8 ef~ | ef4 r4 r2 | r2 r4 r8
  g8~( | g4. f8~ f4. ef8~ | ef4. ef8~ ef4) ef8 ef~ | ef4 r4 r ef8( d8~ | d2) r2
}

altoWords = \lyricmode {
  dim dim dim dim dim dim dim dim dim dim dim dim
  I'll be al -- right.
  My head's un -- der wa -- ter but I'm brea -- thing fine.
  You're cra -- zy and I'm out of my mind.
  Cause all of me loves all of you,
  love your curves and all your edg -- es,
  all your per -- fect im -- per -- fec -- tions.
  Give your all to me, I'll give my all to you.
  You're my end and my be -- gin -- ning.
  E -- ven when I lose, I'm win -- ning.
  all __ of me. all __ of you. oh __
}

tenorMusic = \relative c' {
  % intro
  g4. g g4 af4. af af4 | g4. g g4 f4. f f4
  % verse 1
  r1 r r r r r r r r r r r r r r 
  f2 g8 f4. af1
  % bridge
  af1 bf bf af
  af bf bf2 r4
  % chorus
  g4 | bf2. bf8 bf~ | bf4 r bf2 | bf2. g8 bf8~ | bf4 r
  g8 g4. | af4 af af g8 af | g4 r g8 g4. | af4 af af g8 af | f4 r
  g8 g4. | bf2. bf8 bf~ | bf4 r8 bf bf bf4. | bf2. g8 bf8~ | bf4 r
  g8 g4. | af4 af af g8 af | g4 r g8 g4. | af4 af af g8 af | af4 r r r8
  c8~( | c4. bf8~ bf4.  af8~ | af4. g8~ g4) bf8 bf~ | bf4 r4 r2 | r r4 r8
  c8~( | c4. bf8~ bf4.  af8~ | af4. g8~ g4) bf8 bf~ | bf4 r4 r c8( bf~ | bf2) r
}

tenorWords = \lyricmode {
  \repeat unfold 16 { \skip 1 }
  ooh ooh ooh ooh ooh ooh ooh
}

bassMusic = \relative c {
  ef4. ef ef4 ef4. ef ef4 | ef4. ef ef4 d4. d d4
  r1 r r r r r r r r r r r r r r 
  f2 g8 f4. f1
  % bridge
  f1 ef bf f'
  f ef bf2 r4
  % chorus
  g'4 | ef2. ef8 ef~ | ef4 r ef2 | c2. c8 c~ | c4 r
  g'8 g4. | f4 f f ef8 f | ef4 r g8 g4. | f4 f f ef8 f | f4 r
  g8 g4. | ef2. ef8 ef8~ | ef4 r8 ef8 g ef4. | c2. c8 c8~ | c4 r
  g'8 g4. | f4 f f ef8 f | ef4 r g8 g4. | f4 f f ef8 f | f4 r r r8
  c'8~( | c4. bf8~ bf4.  af8~ | af4. g8~ g4) ef8 ef~ | ef4 r4 r2 | r r4 r8
  c'8~( | c4. bf8~ bf4.  af8~ | af4. g8~ g4) ef8 ef~ | ef4 r4 r ef8( bf~ | bf2) r
}


\score {
  <<
    \new ChoirStaff <<
      \new Lyrics = "sopranos" \with {
        % this is needed for lyrics above a staff
        \override VerticalAxisGroup.staff-affinity = #DOWN
      }
      \new Staff = "women" <<
        \new Voice = "sopranos" {
          \voiceOne
          << \global \sopMusic >>
        }
        \new Voice = "altos" {
          \voiceTwo
          << \global \altoMusic >>
        }
      >>
      \new Lyrics = "all"
      \new Lyrics = "tenors" \with {
        % this is needed for lyrics above a staff
        \override VerticalAxisGroup.staff-affinity = #DOWN
      }
      \new Staff = "men" <<
        \clef bass
        \new Voice = "tenors" {
          \voiceOne
          << \global \tenorMusic >>
        }
        \new Voice = "basses" {
          \voiceTwo
          << \global \bassMusic >>
        }
      >>
      \context Lyrics = "sopranos" \lyricsto "sopranos" \sopWords
      \context Lyrics = "all" \lyricsto "altos" \altoWords
      \context Lyrics = "tenors" \lyricsto "tenors" \tenorWords
    >>
    
    \new Voice = SoloVoiceOne << \global \soloMusic >>
    \new Lyrics \lyricsto "SoloVoiceOne" \soloWords
  >>
  \layout {
    %ragged-right = ##t
    \context {
      \Staff
      \RemoveEmptyStaves
      \override VerticalAxisGroup.remove-first = ##t
    }
  }
}