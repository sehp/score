\version "2.18.2"
\language "english"

\header {
  title = "Bold Riley Solo"
  subsubtitle = ""
  composer = ""
}

global = {
  \time 2/4
  \key c \major
  \tempo 4=80
}

chordNames = \chordmode {
  \set chordChanges = ##t

}

melody = \relative c'' {
  \global
  r4 g a4. g8 | e2 \tuplet 3/2 {e8 d c(} c4)
  r4 g' a4. g16 e | d4 c8 d( d2)
  r4 d8 e f4. f8 e4. d16 e f4. g8 g2 \break
  
  r4 g' a4. g8 e2 \tuplet 3/2 {e8 d c(} c4)
  r8 r16 e g4 a4( a16) g e8 d4.
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff {
      \melody
    }
  >>
  \layout { }
  \midi {
    \tempo 4 = 120
  }
}