\version "2.18.2"
\language "english"

\header {
  title = "Young Edwin in the Lowlands Low"
  composer = "From England"
}

global = {
  \time 4/4
  \key e \minor
  \tempo 4=100
}

chordNames = \chordmode {
  \set chordChanges = ##t
  e2.:m d2 g d | b1:m e2.:m e1:m
  g1 g2 d g1 b:m |
  e2:m d g d | b1:m e:m
}

melody = \relative c' {
  \global
  \repeat volta 2 { \partial 4 b4 | e4. g8 fs4 a | g e d4. e8 | b4. a8 b4 d | } 
  \alternative{
    {
      \set Timing.measureLength = #(ly:make-moment 3/4)
      e2 r4 | 
    }
    {
      \set Timing.measureLength = #(ly:make-moment 4/4)
      e2 r4 fs | 
    }
  }
  g4 d g a | b g a g8( a) | b4. a8 g4 a | b,2 r4 b
  e4. g8 fs4 a | g e d4. e8 | b8\( b4\) a8 b4 d | e2 r
  
  
}

words = \lyricmode {
  \set stanza = "1."
  Come all you wild young pe -- eo -- ple and lis -- ten to my song:
  wrong. Young Em -- ma was a ser -- vant maid, she loved a sai -- lor bold
  He ploughed the main much gold to gain for his love so we’ve been told. 
}
wordy = \lyricmode {
  \set stanza = ""
  Which I un -- fold con -- cer -- ning gold that guides so ma -- ny _ 
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
    \addlyrics { \words }
    \addlyrics { \wordy }
  >>
  \layout { }
  \midi { }
}
\markup { \column{
  \vspace #1
  \line{ \bold 2. }
  \line{ He ploughed the main for seven years and then he returned home. }
  \line{ As soon as he set foot on shore unto his love did go. }
  \line{ He went unto Young Emma’s house his gold all for the show, }
  \line{ That he has gained upon the main all in the lowlands low. }
  \vspace #1
  \line{ \bold 3. }
  \line{ ‘My father keeps a public house down by the side of the sea. }
  \line{ You go there and stay the night, and there you wait for me. }
  \line{ I’ll meet you in the morning, but don’t let my parents know }
  \line{ That your name it is Young Edwin that ploughed the lowlands low.’ }
  \vspace #1
  \line{ \bold 4. }
  \line{ Young Edwin he sat drinking till time to go to bed, }
  \line{ He little thought a sword that night would part his body and head. }
  \line{ And Edwin he got into bed and scarcely was asleep }
  \line{ When Emma’s cruel parents soft into his room did creep. }
  \vspace #1
  \line{ \bold 5. }
  \line{ They stabbed him, dragged him out of bed, and to the sea did go, }
  \line{ They sent his body floating down to the lowlands low. }
  \line{ As Emma she laid sleeping, she had a dreadful dream, }
  \line{ She dreamed she saw Young Edwin’s blood, flowing like a stream. }
  \vspace #1
  \line{ \bold 6. }
  \line{ 'Father, where’s the stranger come here last night to stay?’ }
  \line{ 'Oh, he is dead, no tales can tell,’ her father he did say. }
  \line{ 'Then father, cruel father, you will die a public show }
  \line{ For murdering my Edwin who ploughed the lowlands low.’ }
  }
}
