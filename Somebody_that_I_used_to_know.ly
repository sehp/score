\version "2.18.2"
\language "english"
\header {
  title = "Somebody That I Used To Know"
}

global = {
  \key d \minor
  \time 4/4
  \tempo 4 = 130
}
\score {
  \new ChoirStaff <<
    \new Staff <<
      \set Staff.instrumentName = "S"
      \new Voice {
        \relative c' {
          \global
          r1
          r
          c4-. c-. g'-. g-. 
          a8 bf c a g4-. r4 |
          f-. f-. e-. e-.
          d-. d-. c-. r
          c-. c-. g'-. g-. 
          a8 bf c a g4-. r4 |
          f-. f-. e-. e-.
          d-. d-. c-. r
          d-. d-. r e-.|
          d-. d-. r2 
          \bar "||"
        }
        \addlyrics {
          did did did did
          did did did did did
          did did did did 
          did did did
          did did did did
          did did did did did
          did did did did 
          did did did
          did did did
          did did
        }
      }
    >>
    \new Staff <<
      \set Staff.instrumentName = "A1"
      \new Voice {
        \relative c' {
          \global
          r4 a'-. r g-.
          r a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
        }
        \addlyrics {
          du du 
          du du
          du du du
          du du du
          du du du
          du du du
          du du du
          du du du
          du du du
          du du du
          du du du
          du du du
        }
      }
    >>
    \new Staff <<
      \set Staff.instrumentName = "A2"
      \new Voice {
        \relative c {
          \global
          %\clef bass
          r4 f'-. r e-.
          r f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
        }
        \addlyrics {
          du du 
          du du
          du du du
          du du du
          du du du
          du du du
          du du du
          du du du
          du du du
          du du du
          du du du
          du du du
        }
      }
    >>
    \new Staff {
      \set Staff.instrumentName = "B"
      \new Voice {
        \relative c {
          \global
          \clef bass
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
        }
        \addlyrics {
          dum de dum de
          dum de dum de
          dum de dum de
          dum de dum de
          dum de dum de
          dum de dum de
          dum de dum de
          dum de dum de
          dum de dum de
          dum de dum de
          dum de dum de
          dum de dum de
        }
      }
    }
  >>
}