# score

Sheet music, written in LilyPond. Use of an appropriate LilyPond IDE is recommended, for example Frescobaldi. Makes your life a lot easier.

## Some helpful links:

* [LilyPond](http://lilypond.org)
* [Frescobaldi](http://frescobaldi.org/index.html)
* [LilyPond Reference](http://lilypond.org/doc/v2.18/Documentation/learning/index.html)
