\version "2.18.2"
\language "english"

\header {
  title = "Inisheer"
  subsubtitle = ""
  composer = ""
}

global = {
  \time 3/4
  \key g \major
  \tempo 4=80
}

chordNames = \chordmode {
  \set chordChanges = ##t
  g2. e:m c d
  g e:m c2 d4 g2.
  e:m g e2:m g4 e2.:m
  e:m g c4 d2 g1
}

melody = \relative c'' {
  \global
  \repeat volta 2 {
    b4 b8( a b d) b4 b8( a b d) | e,4 e8( b' a b) d,4 d8( b' a d) \break
    b4 b8( a b d) b4 b8( a b d) | g,4 g8( b a b) g2. } \break
  \repeat volta 2 {
    e'4. fs8( e d) b4.( a8) \tuplet 3/2 { b( c d) } | e( fs e d) \tuplet 3/2 { b( c d) } e2 g8( fs) \break
    e4. fs8( e d) b4. a8( b d) | g,4 g8( b a b) g2.
  }
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff {
      \melody
    }
%    \addlyrics { \words }
  >>
  \layout { }
  \midi {
    \tempo 4 = 80
  }
}