\version "2.18.2"
\language "english"

\header {
  title = "Bold Riley"
  subsubtitle = ""
  composer = ""
}

global = {
  \time 2/4
  \key c \major
  \tempo 4=80
}

chordNames = \chordmode {
  \set chordChanges = ##t
  c1. f2 c g1 f2 c f g1
  c f2 c g1 f2 c g c1
  %
  c1 c c g g2 f c f g1
  g c c g g2 f c g c1
}

melodyVerse = \relative c'' {
  \global
  r8 g([ a]) b | c g4 g8~ g2 | f4 g e4.( d16 c | d2) \break
  r4 d8([ e]) f4. f8 \tuplet 3/2 {e( d c)} c4 f4.( g8) g2 \break
  r4 a8 b | c g4 g8~ g4. e8 f4 g e4(~ e16 d16 c8 d2) \break
  r4 d8( e) f4. f8 \tuplet 3/2 {e( d c)} c4 d4. c8 c2~ c2 \bar "||" \break
}

melodyChorus = \relative c'' {
  r4 g4 a8( g4) g8 e2 e16( d c4.)
  r4 g'4 a8( g4) g8 d4.( c8) d2 \break
  r4 d8( e) f4. f8 \tuplet 3/2 {e( d c)} c4 f4.( g8) g2~ g2 \break
  r4 g4 a8( g4) g8 e4.( g8) c,2
  r4 g'4 g8( f4) e8 d4.( c8) d2 \break
  r4 d8( e) f4. f8 \tuplet 3/2 {e( d c)} c4 d4. c8 c2~ c2 \break
}

wordsVerseOne = \lyricmode {
  \set stanza = #"1."
  Oh the rain it rains all day long
  Bold Ri -- ley -- o, Bold Ri -- ley
  And the nor -- thern wind, it blows so strong
  Bold Ri -- ley -- o has gone a -- way
  %
  Good -- bye my sweet -- heart
  Good -- bye my dear -- o
  Bold Ri -- ley -- o, Bold Ri -- ley
  Good -- bye my dar -- lin'
  Good -- bye my dear -- o
  Bold Ri -- ley -- o has gone a -- way
}

wordsChorus = \lyricmode {
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff {
      \new Voice {
        \melodyVerse
        \melodyChorus
      }
      \addlyrics { \wordsVerseOne }
    }
  >>
  \layout { }
  \midi {
    \tempo 4 = 120
  }
}
\markup { \column{
  \vspace #2
  \line{ \bold 2. }
  \line{ Well come on, Mary, don't look so glum }
  \line{ Bold Riley-o, Bold Riley }
  \line{ Come White-stocking Day you'll be drinkin' rum }
  \line{ Bold Riley-o has gone away }
  \line{ Goodbye my sweetheart... }
  }
  \hspace #8
  \column{
    \vspace #2
    \line{ \bold 3. }
    \line{ Our anchor’s aweigh and our sails are all set, }
    \line{ Bold Riley-o, Bold Riley }
    \line{ And those Liverpool girls we’ll never forget, }
    \line{ Bold Riley-o has gone away }
    \line{ Goodbye my sweetheart... }
  }
}