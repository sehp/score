\version "2.18.2"
\language "english"

\header {
  title = "Cooley's Reel"
  %subsubtitle = ""
%  composer = "From Ireland"
}

global = {
  \time 4/4
  \key e \minor
  \tempo 4=130
}

chordNames = \chordmode {
  \set chordChanges = ##t
}

partOne = \relative c' {
  \global
  \repeat volta 2 {
    \partial 4 r8 d | e  b' b a b4 e,8 b' b4 a8 b d b a g
    fs d a' d, b' d, a' d, 
    fs d a' d, d' a fs16 e d8
    %
    e b' b a b4 e,8 b' b4
    a8 b d e fs g a fs e cs d b a fs
    d e fs d e4
  }
  %
  \repeat volta 2 {
    g'8 fs  \break e b b4 e8 fs g e e b b4
    g'8 e d b a4 fs8 a d, a' fs a a4
    fs8 a d e fs g e b b4 e8 b g' b, e b b4
    d8 e fs g a fs e cs d b a fs d e fs d e4
  }
}

\score {
  <<
    \new Staff {
      \partOne
    }
  >>
  \layout { }
  \midi { }
}