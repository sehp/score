\version "2.18.2"
\language "english"

\header {
  title = "Never set the cat on fire"
  subsubtitle = "Instructions for a child living on a spaceship"
  composer = "Frank Hayes"
}

global = {
  \time 4/4
  \key d \major
  \tempo 4=130
}

chordNames = \chordmode {
  \set chordChanges = ##t
  d1 d a d
  d d g a
  g d a a
  a a d
  g2 a d1 g
  a a a d
}

melodyVerse = \relative c' { 
  \global
  d4. e8 fs4 g a b a4. fs8 g4 fs e a fs d r 
  d d4. e8 fs4 g a b a4. a8 b4 a g fs fs e r
  e b'4. a8 g4 b a g fs4. a8 g4 fs e d e fs g
  e a4. b8 a4 g fs4-. r e-. r
  \set Timing.measureLength = #(ly:make-moment 7/8) d2 r4. \break
  \set Timing.measureLength = #(ly:make-moment 4/4)
}

melodyChorus = \relative c' {
  \set Timing.measureLength = #(ly:make-moment 1/8) d8 
  \set Timing.measureLength = #(ly:make-moment 4/4) b'2 cs | d4 a r a | b a g fs \break e fs g e 
  a4. b8 a4 g fs-. r e-. r d2 r
}

wordsVerseOne = \lyricmode {
  \set stanza = #"1. "
  Ne -- ver set the cat on fire, you on -- ly will an -- noy it.
  The heat will make the beast per -- spire; it sure -- ly won't en -- joy it.
  Like -- wise do not ig -- nite the dog, the snake, the ger -- bil, or the frog,
  no, ne -- ver set the cat on fire
  %
  And mind your man -- ners, as cir -- cum -- stan -- ces may re -- quire
  and ne -- ver set the cat on fire.
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff {
      \melodyVerse
      \melodyChorus
    }
    \addlyrics { \wordsVerseOne }
  >>
  \layout { }
  \midi {
    \tempo 4 = 90
  }
}

\markup { \column{
  \vspace #2
  \line{ \bold 2. }
  \line{ Don't open up the cabin hatch; the air is sure to leave it }
  \line{ And air is very hard to catch; you never will retrieve it }
  \line{ And even though your life's a bore, don't open the reactor door }
  \line{ Don't open up the cabin hatch }
  \line{ And mind your manners... }
  \vspace #1
  \line{ \bold 3. }
  \line{ Don't change the navigator's data, someone's sure to see ya }
  \line{ You know the captain's view of that: A very bad idea! }
  \line{ He doesn't want his ship to race forever lost in endless space }
  \line{ Don't change the navigator's data }
  \line{ And mind your manners... }
  \vspace #1
  \line{ \bold 4. }
  \line{ Don't start an interstellar war; it has no helpful uses }
  \line{ When someone asks you 'what's it for?', you'll only make excuses }
  \line{ If thirty trillion folks get hurt, you'll go to bed with no dessert! }
  \line{ Don't start an interstellar war }
  \line{ And mind your manners... }
  }
}


% 
% 
% 
% 
% When someone asks you 'what's it for?', you'll only make excuses
% If thirty trillion folks get hurt, you'll go to bed with no dessert!
% Don't start an interstellar war