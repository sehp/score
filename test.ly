\version "2.18.2"
\language "english"
\score {
  \new ChoirStaff <<
    \new Staff {
      \set Staff.instrumentName = "S"
      \new Voice {
        \relative c' {
          \key d \minor
          r1
          r
          c4-. c-. g'-. g-. |
          a8 b c a g4-. r4 |
          f-. f-. e-. e-.
          d-. d-. c-. r
          c-. c-. g'-. g-. 
          a8 b c a g4-. r4 |
          f-. f-. e-. e-.
          d-. d-. c-. r
          d-. d-. r e-.|
          d-. d-. r2 
          \bar "||"
        }
      }
    }
    \new Staff {
      \set Staff.instrumentName = "A1"
      \new Voice {
        \relative c' {
          \key d \minor
          r4 a'-. r g-.
          r a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
          a-. a-. r g-.
        }
      }
    }
    \new Staff {
      \set Staff.instrumentName = "A2"
      \new Voice {
        \relative c {
          \key d \minor
          %\clef bass
          r4 f'-. r e-.
          r f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
          f-. f-. r e-.
        }
      }
    }
    \new Staff {
      \set Staff.instrumentName = "B"
      \new Voice {
        \relative c {
          \key d \minor
          \clef bass
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
          d4 r8 d8 c4 r8 c8
        }
      }
    }
  >>
}