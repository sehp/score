\version "2.18.2"
\language "english"
\include "Snippet_swing.ly"

\header {
  title = "Sera was never"
  subsubtitle = "From Dragon Age: Inquisition"
  composer = "Raney Shockne"
}

global = {
  \time 4/4
  \key a \minor
  % \tempo 4=1001
  \rhythmMark #"" \rhyMarkIIEighths \rhyMarkTriplets
}

chordNames = \chordmode {
  \set chordChanges = ##t
  a1:m a:m
  a1:m e2:m g a1:m a:m
  a:m e2:m g a1:m a:m
  %%%%%
  a:m e2:m g a1:m a:m
  c g a:m a:m
}

intro = {
  \global
  r1 | r
}

melodyVerse = \relative c'' {
  \repeat volta 2 { \mark \markup { \italic { Verse } } e4 e8 d e fs g e | d b a4 g r8 b
  c4 a e'8 d c4 | a4\( e a\) a8 c
  e4 a2 a8 g | e d b4 g r8 b
  c4 a e'8 d c4 | a\( e a\) r } \break 
}

melodyChorus = \relative c'' {
  \repeat volta 2 { \mark \markup { \italic { Chorus } } e4. fs8 g4. a8 | g4 e d2
  e4 a8 g a g a b | a4 g e fs
  g g8 a g4 e | d b g r8 b
  c4 a8 a e' d c4 | a e a r  }
}

wordsVerseOne = \lyricmode {
  \set stanza = #"1. "
  Se -- ra was ne -- ver an a -- gree -- a -- ble girl,
  her tongue tells tales of re -- be -- ll -- ion.
  But she was so fast and quick with her bow,
  no one quite knew where she ca -- ame from
  
  She would al -- ways like to say
  Why change the past when you can own this day?
  To -- day she will fight to keep her way.
  A rogue and a thief and she'll tempt your fate
}
wordsVerseTwo = \lyricmode {
  \set stanza = #""
  Se -- ra was ne -- ver quite the qui -- e -- test girl,
  her attacks are loud and they're jo -- oy -- ful.
  But she knew the ways of no -- be -- ler men,
  And she knew how to en -- ra -- age them.
}

wordsChorus = \lyricmode {
  \unset stanza
  She would al -- ways like to say
  Why change the past when you can own this day?
  To -- day she will fight to keep her way
  A rogue and a thief and she'll tempt your fate
}

\score {
  <<
    \new ChordNames \chordNames
    %\new FretBoards \chordNames
    \new Staff { 
      \intro
      \melodyVerse
      \melodyChorus
    }
    \addlyrics { \wordsVerseOne }
    \addlyrics { \wordsVerseTwo }
    %\addlyrics { \wordsChorus }
    
  >>
  \layout { }
  \midi {
    \tempo 4 = 120
  }
}
\markup { \column{
  \vspace #2
  \line{ \bold 2. }
  \line{ Sera was never quite the wealthiest girl }
  \line{ Some say she lives in a tavern. }
  \line{ But she was so sharp and quick with her bow }
  \line{ Arrows strike like a dragon. }
  \vspace #1
  \line{ Sera was never quite the gentlest girl }
  \line{ Her eyes were sharp like a razor. }
  \line{ But she knew the ways of commoner men }
  \line{ And she knew just how to use them. }
  }
}