\version "2.18.2"
\language "english"

\header {
  title = "Leave her, Johnny"
  composer = "Traditional"
}

global = {
  %\time 4/4
  \key d \major
  %\tempo 4=100
  \partial 4 
}

chordNames = \chordmode {
  \set chordChanges = ##t
  
}

melody = \relative c' {
  \global
  \mark \markup { \italic { Verse } } \partial 4 d8( e) | fs4 fs fs e8( d) | fs4 e d2
  <<{e4 e e4. d8 | fs( a) a4 r} \\ {b, b b4. b8 | b8( e) e4 r}>>
  d'8( cs) | b4 b a fs | g g fs 
  d8 e | fs4. g8 fs4. e8 | e4 d2 r4
  \break
  \mark \markup { \italic { Chorus } } e4 e e4. d8 | fs( a) a4 r4 d8( cs) | b4 b b4. a8 | a4 fs r4 d'8 cs
  b4 b a fs8 a | b4 a fs d8 e | fs4. a8 fs4. e8 | e4 d2 r4
}

mainMelody = \relative c' {
  d8( e) | fs4 fs fs e8( d) | fs4 e d2
  e4 e e4. d8 | fs( a) a4 r
  d8( cs) | b4 b a fs | g g fs 
  d8 e | fs4. g8 fs4. e8 | e4 d2 r4
  \break
  e e4. d8 | fs( a) a4 r4 d8( cs) | b4 b b4. a8 | a4 fs r4 d'8 cs
  b4 b a fs8 a | b4 a fs d8 e | fs4. a8 fs4. e8 | e4 d2 r4
}

bassMelody = \relative c' {
  d8( e) | fs4 fs fs e8( d) | fs4 e d2
  b4 b b4. b8 | b8( e) e4 r
  d'8( cs) | b4 b a fs | g g fs 
  d8 e | fs4. g8 fs4. e8 | e4 d2 r4
  \break
  e4 e e4. d8 | fs( a) a4 r4 d8( cs) | b4 b b4. a8 | a4 fs r4 d'8 cs
  b4 b a fs8 a | b4 a fs d8 e | fs4. a8 fs4. e8 | e4 d2 r4
}

wordsVerse = \lyricmode {
  \set stanza = "1."
  I_ | thought I heard the_ | old man say: |
  Leave her, Joh -- nny, | leave_ her! 
  To_ -- mor -- row you will | get your pay
  And it's | time for us to | leave her.
  
  Leave her, Joh -- nny, leave her,
  oh leave her, Joh -- nny, leave her!
  For the voyage is done and the winds don't blow
  And it's time for us to leave her.
}
wordsChorus = \lyricmode {
}

\score {
    %\new ChordNames \chordNames
  \new ChoirStaff <<
    \new Staff <<
      \new Voice = "Main" { \voiceOne \global \mainMelody }
      \new Voice = "Bass" { \voiceTwo \bassMelody }
    >>
    \new Lyrics \lyricsto "Main" { \wordsVerse }
  >>
  \layout { }
  \midi { }
}
\markup { \column{
  \vspace #1
  \line{ \bold 2. }
  \line{ Oh, the wind was foul and the sea ran high }
  \line{ Leave her, Johnny, leave her! }
  \line{ She shipped it green and none went by }
  \line{ And it's time for us to leave her. }
  \vspace #1
  \line{ \bold 3. }
  \line{ I hate to sail on this rotten tub }
  \line{ Leave her, Johnny, leave her! }
  \line{ No grog allowed and rotten grub }
  \line{ And it's time for us to leave her. }
  \vspace #1
  \line{ \bold 4. }
  \line{ We swear by rote for want of more }
  \line{ Leave her, Johnny, leave her! }
  \line{ But now we're through so we'll go on shore }
  \line{ And it's time for us to leave her. }
  }
}
