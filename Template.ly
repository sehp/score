\version "2.18.2"
\language "english"

\header {
  title = ""
  subsubtitle = ""
  composer = ""
}

global = {
  \time 4/4
  \key a \minor
  \tempo 4=100
}

chordNames = \chordmode {
  \set chordChanges = ##t

}

melody = \relative c' {
  
}

words = \lyricmode {
  \set stanza = #"1."
  
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff {
      \melody
    }
    \addlyrics { \words }
  >>
  \layout { }
  \midi {
    \tempo 4 = 120
  }
}
\markup { \column{
  \vspace #2
  \line{ \bold 2. }
  \line{ asdf }
  }
}