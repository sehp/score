\version "2.18.2"
\language "english"

\header {
  title = "The Foggy Dew"
  %subsubtitle = ""
  composer = "From Ireland"
}

global = {
  \time 2/4
  \key e \minor
  \tempo 4=60
}

chordNames = \chordmode {
  \set chordChanges = ##t
}

partOne = \relative c'' {
  \global
  \partial 4 b8 d | e4 d8 b e4 d8 b a4. b8 d,4. e16 fs
  g8 b a g | e4. d8 d8( e4.) r4 b'8 d d( e) d b d( e) d b a4. b8 d,4. e16 fs
}

\score {
  <<
    \new Staff {
      \partOne
    }
  >>
  \layout { }
  \midi { }
}